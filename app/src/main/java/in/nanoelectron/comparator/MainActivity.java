package in.nanoelectron.comparator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView result;
    private EditText a,b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a = (EditText) findViewById(R.id.a);
        b = (EditText) findViewById(R.id.b);
        result = (TextView) findViewById(R.id.result);

        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String first  = a.getText().toString();
                String second  = b.getText().toString();

                if(!(first.isEmpty() && second.isEmpty())) {
                    try {
                        int firstDecimal = Integer.parseInt(first, 2);
                        int secondDecimal = Integer.parseInt(second, 2);

                        if (firstDecimal > secondDecimal) {
                            result.setText("A is Greater than B");
                        } else if (firstDecimal < secondDecimal) {
                            result.setText("B is Greater than A");
                        } else {
                            result.setText("both are equal");
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                        result.setText("Enter Only Binary Values");
                        Toast.makeText(getBaseContext(),"Enter only Binary Values",Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                    result.setText("enter values");
                }

            }
        });
    }
}
